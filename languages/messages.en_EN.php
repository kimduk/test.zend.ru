<?php
return array(
    'menu-home' => 'HOME',
    'menu-services' => 'SERVICES',
    'menu-catalog' => 'CATALOG',
    'menu-contact' => 'CONTACT',
    'welcome' => 'Welcome',
    'created-with' => 'Created with',
    'licensed-under' => 'License',
    'contact-name' => 'Name:',
    'contact-email-address' => 'Address email: ',
    'contact-message' => 'Message:',
    'contact-verification' => 'Verification',
    'contact-send-message' => 'Send message',
    'contact-title' => 'Title'
);