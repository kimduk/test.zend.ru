<?php
return array(
    'menu-home' => 'HOME',
    'menu-services' => 'DIENSTLEISTUNGEN',
    'menu-catalog' => 'CATALOG',
    'menu-contact' => 'KATALOG',
    'menu-search' => 'SUCHE',
    'menu-news' => 'Nachrichten',
    'welcome' => 'Willkommen',
    'created-with' => 'Erstellt mit',
    'licensed-under' => 'Lizenz',
    'licensed-under' => 'Lizenz',
    'contact-name' => 'Name:',
    'contact-email-address' => 'Address email: ',
    'contact-message' => 'Message:',
    'contact-verification' => 'Verification',
    'contact-send-message' => 'Send message',
    'contact-title' => 'Title'
);