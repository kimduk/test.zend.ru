<?php
return array(
    'menu-home' => 'ACCUEIL',
    'menu-services' => 'SERVICES',
    'menu-catalog' => 'BROCHURE',
    'menu-search' => 'Rechercher',
    'menu-news' => 'Nouvelles',
    'menu-contact' => 'CONTACTEZ-NOUS',
    'welcome' => 'Bienvenue',
    'created-with' => 'Cree avec',
    'licensed-under' => 'Sous license',
    'contact-name' => 'Nom:',
    'contact-email-address' => 'Adresse email: ',
    'contact-message' => 'Message:',
    'contact-verification' => 'Verification',
    'contact-send-message' => 'Envoyer Message',
    'contact-title' => 'Contactez-Nous'
);