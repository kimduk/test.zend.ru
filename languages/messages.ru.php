<?php
return array(
    'menu-home' => 'Главная',
    'menu-services' => 'Сурвис',
    'menu-catalog' => 'Католог',
    'menu-contact' => 'Контакт',
    'welcome' => 'ДОбро пожаловаь',
    'created-with' => 'Создано',
    'licensed-under' => 'Лицензия',
    'contact-name' => 'Имя:',
    'contact-email-address' => 'Имейл адрес: ',
    'contact-message' => 'Сообщение:',
    'contact-verification' => 'Верификация',
    'contact-send-message' => 'Отправить сообщение',
    'contact-title' => 'Заголовок'
);