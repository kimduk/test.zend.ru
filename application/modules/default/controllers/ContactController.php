<?php
class ContactController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->doctype('XHTML1_STRICT');
    }

    public function indexAction()
    {
        $form = new Square_Form_Contact();
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $values = $form->getValues();
                $configs = $this->getInvokeArg('bootstrap')->getOptions('configs');
                $localConfig = new Zend_Config_Ini($configs['localConfigPath']);
                $to = (!empty($localConfig->user->salerEmailAddress)) ? $localConfig->user->salerEmailAddress
                    : $localConfig->global->defaultEmailAddress;
                $mail = new Zend_Mail();
                $mail->setBodyText($values['message']);
                $mail->setFrom($values['email'] . $values['name']);
                $mail->addTo($to);
                $mail->setSubject('Contact from submission');
                $mail->send();
                $this->_helper->getHelper('FlashMessenger')->addMessage('Thanks. Your message was successfully send.');
                $this->_redirect('/contact/success');

            }
        }

        $formJQuery = new ZendX_JQuery_Form();
        $subFormJQuery1 = new ZendX_JQuery_Form();
        $subFormJQuery2 = new ZendX_JQuery_Form();

        $formJQuery->addSubForm($subFormJQuery1, 'subform1');
        $formJQuery->addSubForm($subFormJQuery2, 'subform2');

        $formJQuery->setDecorators(
            array(
                 'FormElements',
                 array('TabContainer', array('id' => 'tabContainer', 'style' => 'width: 600px;'), 'Form')
            )
        );

//        $subFormJQuery1->setDecorators(
//            array(
//                 'FormElements',
//                 array('HtmlTag', array('tag' => 'dl')),
//                 array('TabPane',
//                       array('jQueryParams' => array('containerId' => 'mainForm', 'title' => 'DatePicker'))
//                 ))
//        );

//        $subFormJQuery2->setDecorators(
//            array(
//                 'FormElements',
//                 array('HtmlTag', array('tag' => 'dl')),
//                 array('TabPane',
//                       array('jQueryParams' => array('containerId' => 'mainForm', 'title' => 'AutoComplete and Slider'))
//                 ))
//        );
//
        $subFormJQuery1->addElement(
            'DatePicker', 'datePicker1',
            array(
                 'label'       => 'Date Pecker:',
                 'JQueryParams' => array('dateFormat' => 'dd.mm.yy')
            )
        );

//        $subFormJQuery2->addElement(
//            'Slider', 'slider1',
//            array(
//                 'label'       => 'Slider:',
//                 'JQueryParams' => array('defaultValue' => '75')
//            )
//        );

        $subFormJQuery2->addElement(
            'AutoComplete', 'ac1',
            array(
                 'label'       => 'AutoComplete:',
                 'JQueryParams' => array('source' => array('New York', 'Berlin', 'Bern', 'Boston'))
            )
        );

        $this->view->formJQuery = $formJQuery;
        $this->view->subFormJQuery1 = $subFormJQuery1;
    }

    public function autocompleteAction()
    {
        $this->_helper->layout->disableLayout();
        $this->getHelper('viewRenderer')->setNoRender(true);

        $territories = Zend_Locale::getTranslationList('territory', null, 2);
        $items = array();
        foreach ($territories as $t) {
            $items[] = array('name' => $t);
        }

        $data = new Zend_Dojo_Data('name', $items);
        header('Content-Type: application/json');
        echo $data->toJson();
    }

    public function successAction()
    {
        if ($this->_helper->getHelper('FlashMessenger')->getMessages()) {
            $this->view->messager = $this->_helper->getHelper('FlashMessenger')->getMessages();
        } else {
            $this->_redirect('/');
        }
    }
}