<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->title = 'Page Not Found';
                $this->view->message = 'The requst page could not be found.';
                $priority = Zend_Log::NOTICE;
//                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->title = 'Internal Server Error';
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;

        $logger = new Zend_Log();

        $config = $this->getInvokeArg('bootstrap')->getOption('logs');
        $xmlWriter = new Zend_Log_Writer_Stream(
            $config['logPath'] . '/error.log.xml'
        );
        $logger->addWriter($xmlWriter);
        $formatter = new Zend_Log_Formatter_Xml();
        $xmlWriter->setFormatter($formatter);

        $columnMap = array(
            'message' => 'LogMessage',
            'priorityName' => 'LogLevel',
            'timestamp' => 'LogTime',
            'stacktrace' => 'Stack',
            'request' =>'Request'
        );

        $dbWriter = new Square_Log_Writer_Doctrine('Square_Model_Log', $columnMap);
        $logger->addWriter($dbWriter);

        $fbWriter = new Zend_Log_Writer_Firebug();
        $logger->addWriter($fbWriter);

        $logger->setEventItem('stacktrace' , $errors->exception->getTraceAsString());
        $logger->setEventItem('request' , Zend_Debug::dump($errors->request->getParams()));

        $logger->log($errors->exception->getMessage(), Zend_Log::ERR);

    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

