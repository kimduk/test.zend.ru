<?php
class Catalog_AdminItemController extends Zend_Controller_Action
{
    public function preDispatch()
    {
        $url = $this->getRequest()->getRequestUri();
        $this->_helper->layout->setLayout('admin');
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $session = new Zend_Session_Namespace('square.auth');
            $session->requestURL = $url;
            $this->_redirect('/admin/login');
        }
    }

    public function indexAction()
    {
        $filters = array(
            'page' => array('HtmlEntities', 'StringTrim', 'StripTags'),
            'sort' => array('HtmlEntities', 'StringTrim', 'StripTags'),
            'dir' => array('HtmlEntities', 'StringTrim', 'StripTags')
        );
        $validators = array(
            'page' => array('Int'),
            'sort' => array('Alpha', array('InArray', 'haystack' => array('RecordID', 'Title', 'Denomination', 'CountryID', 'GradeID', 'Year'))),
            'dir' => array('Alpha', array('InArray', 'haystack' => array('asc', 'desc')))
        );
        $input = new Zend_Filter_Input($filters, $validators);
        $input->setData($this->getRequest()->getParams());
        if($input->isValid()) {
            $q = Doctrine_Query::create()
                ->from('Square_Model_Item i')
                ->leftJoin('i.Square_Model_Grade g')
                ->leftJoin('i.Square_Model_Country c')
                ->leftJoin('i.Square_Model_Type t')
            ->orderBy(sprintf('%s %s', $input->sort, $input->dir));

            $perPage = 4;
            $numPageLinks = 5;

            $pager = new Doctrine_Pager($q, $input->page, $perPage);
            $result = $pager->execute(array(), Doctrine::HYDRATE_ARRAY);

            $pageRange = new Doctrine_Pager_Range_Sliding(
                array('chunk' => $numPageLinks), $pager
            );

            $pagerUrlBase = $this->view->url(
                array(), 'admin-catalog-index', 1
            ) . "/{%page}/{$input->sort}/{$input->dir}";
            $pagerLayout = new Doctrine_Pager_Layout(
                $pager, $pageRange, $pagerUrlBase
            );
            $pagerLayout->setTemplate('<a href="{%url}">{%page}</a>');
            $pagerLayout->setSelectedTemplate(
                '<span class="current">{%page}</span> '
            );
            $pagerLayout->setSeparatorTemplate('&nbsp;');

            $this->view->pages = $pagerLayout->display(null, true);
            $this->view->records = $result;
        } else {
            throw new Zend_Controller_Action_Exception('Invalid input');
        }
    }

    public function deleteAction()
    {
        $filters = array(
            'ids' => array('HtmlEntities', 'StripTags', 'StringTrim')
        );
        $validators = array(
            'ids' => array('NotEmpty', 'Int')
        );

        $input = new Zend_Filter_Input($filters, $validators);
        $input->setData($this->getRequest()->getParams());

        if ($input->isValid()) {
            $q = Doctrine_Query::create()
                ->delete('Square_Model_Item i')
                ->whereIn('i.RecordID', $input->ids);
            $result = $q->execute();

            $config = $this->getInvokeArg('bootstrap')->getOptions('uploads');
            foreach($input->ids as $id) {
                foreach(glob("{$config['uploadPath']}/{$id}_*") as $file) {
                    unlink($file);
                }
            }

            $this->_helper->getHelper('FlashMessenger')->addMessage('The record were successfully deleted. ');
            $this->_redirect('/admin/catalog/item/success');
        } else {
            throw new Zend_Controller_Action_Exception('Invalid input');
        }
    }

    public function updateAction()
    {

        $this->view->headLink()->appendStylesheet('http://yui.yahooapis.com/combo?2.8.0r4/build/calendar/assets/skins/sam/calendar.css');
        $this->view->headScript()->appendFile('http://yui.yahooapis.com/combo?2.8.0r4/build/yahoo-dom-event/yahoo-dom-event.js&2.8.0r4/build/calendar/calendar-min.js');
        $form = new Square_Form_ItemUpdate();
        $this->view->form = $form;

        if($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();

//            $postData['DisplayUntil'] = sprintf('%04d-%02d-%02d',
//                $postData['DisplayUntil_year'],
//                $postData['DisplayUntil_month'],
//                $postData['DisplayUntil_day']
//            );

            if($form->isValid($postData)) {
                $input = $form->getValues();
                $item = Doctrine::getTable('Square_Model_Item')
                    ->find($input['RecordID']);
                $item->fromArray($input);
                $item->DisplayUntil = ($item->DisplayStatus == 0) ? null : $item->DisplayUntil;
                $item->save();
                $this->_helper->getHelper('FlashMessenger')->addMessage('The record was successfully update');
                $this->_redirect('/admin/catalog/item/success');
            } else {
                throw new Zend_Controller_Action_Exception('Invalid input data');
            }
        } else {
            $filter = array(
                'id' => array('HtmlEntities', 'StringTrim', 'StripTags')
            );
            $validator = array(
                'id' => array('NotEmpty', 'Int')
            );
            $input = new Zend_Filter_Input($filter, $validator);
            $input->setData($this->getRequest()->getParams());
            if($input->isValid()) {

                $memoryCache = $this->getInvokeArg('bootstrap')
                    ->getResource('cachemanager')
                    ->getCache('memory');

                if(!($result = $memoryCache->load('public_item_' . $input->id))) {
                    $item = new Square_Model_Item;
                    $result = $item->getItem($input->id, false);
                    $memoryCache->save($result, 'public_item_' . $input->id);
                }

                if(count($result) == 1) {

//                    $date = $result[0]['DisplayUntil'];
//                    $result[0]['DisplayUntil_day'] = date('d', strtotime($date));
//                    $result[0]['DisplayUntil_month'] = date('m', strtotime($date));
//                    $result[0]['DisplayUntil_year'] = date('Y', strtotime($date));

                    $this->view->form->populate($result[0]);
                } else {
                    throw new Zend_Controller_Action_Exception('Page not found', 404);
                }
            } else {
                throw new Zend_Controller_Action_Exception('Invalid input');
            }
        }
    }

    public function successAction()
    {
        if($this->_helper->getHelper('FlashMessenger')->getMessages()) {
            $this->view->messages = $this->_helper->getHelper('FlashMessenger')->getMessages();
        } else {
            $this->_redirect('/admin/catalog/item/index');
        }
    }

    public function createFulltextIndexAction()
    {
        $q = Doctrine_Query::create()
            ->from('Square_Model_Item i')
            ->leftJoin('i.Square_Model_Grade g')
            ->leftJoin('i.Square_Model_Country c')
            ->leftJoin('i.Square_Model_Type t')
            ->where('i.DisplayStatus = 1')
            ->addWhere('i.DisplayUntil >= CURDATE()');

        $result = $q->fetchArray();

        $config = $this->getInvokeArg('bootstrap')->getOption('indexes');
        $index = Zend_Search_Lucene::create($config['indexPath']);
        foreach($result as $r) {
            $doc = new Zend_Search_Lucene_Document();
            $doc->addField(Zend_Search_Lucene_Field::Text('Title', $r['Title']));
            $doc->addField(Zend_Search_Lucene_Field::Text('Country', $r['Square_Model_Country']['CountryName']));
            $doc->addField(Zend_Search_Lucene_Field::Text('Grade', $r['Square_Model_Grade']['GradeName']));
            $doc->addField(Zend_Search_Lucene_Field::Text('Year', $r['Year']));
            $doc->addField(Zend_Search_Lucene_Field::UnStored('Description', $r['Description']));
            $doc->addField(Zend_Search_Lucene_Field::UnStored('Denomination', $r['Denomination']));
            $doc->addField(Zend_Search_Lucene_Field::UnStored('Type', $r['Square_Model_Type']['TypeName']));
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('SalePriceMin', $r['SalePriceMin']));
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('SalePriceMax', $r['SalePriceMax']));
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('RecordID', $r['RecordID']));
            $index->addDocument($doc);
        }

        $count = $index->count();
        $this->_helper->getHelper('FlashMessenger')->addMessage("The index was successful created with $count documents");
        $this->_redirect('/admin/catalog/item/success');
    }

}