<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctrine()
    {
        require_once 'Doctrine/Doctrine.php';
        $this->getApplication()
            ->getAutoloader()
            ->pushAutoloader(array('Doctrine', 'autoload'), 'Doctrine');

        $manager = Doctrine_Manager::getInstance();
        $manager->setAttribute(
            Doctrine::ATTR_MODEL_LOADING,
            Doctrine::MODEL_LOADING_CONSERVATIVE
        );
        $config = $this->getOption('doctrine');
        $conn = Doctrine_Manager::connection($config['dsn'], 'doctrine');
        return $conn;
    }

    protected function _initLocale()
    {
        $session = new Zend_Session_Namespace('square.110n');
        $locale = null;
        if($session->locale) {
            $locale = new Zend_Locale($session->locale);
        }
        if($locale === null) {
            try {
                $locale = new Zend_Locale('browser');
            } catch (Zend_Locale_Exception $e) {
                $locale = new Zend_Locale('en_GB');
            }
        }
        $registry = Zend_Registry::getInstance();
        $registry->set('Zend_Locale', $locale);
    }

    protected function _initTranslation()
    {
        $translate = new Zend_Translate('array',
            APPLICATION_PATH . '/../languages/', null, array('scan'           => Zend_Translate::LOCALE_FILENAME,
                                                             'disableNotices' => 1));
        $registry = Zend_Registry::getInstance();
        $registry->set('Zend_Translate', $translate);
    }

    protected function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $restRoute = new Zend_Rest_Route($front, array(), array('api'));
        $router->addRoute('api', $restRoute);
    }

    protected function _initNavigation()
    {
        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml');
        $container = new Zend_Navigation($config);

        $registry = Zend_Registry::getInstance();
        $registry->set('Zend_Navigation', $container);

        Zend_Controller_Action_HelperBroker::addHelper(
            new Square_Controller_Action_Helper_Navigation()
        );
    }

    protected function _initJQuery()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->addHelperPath('ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');

        $view->jQuery()
            ->enable()
            ->uiEnable()
            ->setLocalPath('/js/jquery-1.9.1.js')
            ->addStylesheet('/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')
            ->setUiLocalPath('/js/jquery-ui-1.10.3.custom.min.js');

    }

//    protected function _initDojo()
//    {
//        $this->bootstrap('view');
//        $view = $this->getResource('view');
//
//        Zend_Dojo::enableView($view);
//
//        $view->dojo()->setCdnBase(Zend_Dojo::CDN_BASE_AOL)
//            ->addStyleSheetModule('dijit.themes.tundra')
//            ->disable();
//    }

    protected function _initCache()
    {
        $this->bootstrap('cachemanager');
        $manager = $this->getResource('cachemanager');
        $memoryCache = $manager->getCache('memory');
        Zend_Locale::setCache($memoryCache);
        Zend_Translate::setCache($memoryCache);
    }
}

