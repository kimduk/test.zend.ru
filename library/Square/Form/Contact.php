<?php
class Square_Form_Contact extends Zend_Dojo_Form
{
    public function init()
    {
        $this->setAction('/contact')
            ->setMethod('post');

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('contact-name')
            ->setOptions(array('size' => '35'))
            ->setRequired(true)
            ->addValidator('NotEmpty', true)
            ->addValidator(
                'Alpha', true,
                array('messages' =>
                      array(
                          Zend_Validate_Alpha::INVALID      => "ERROR: Invalid name",
                          Zend_Validate_Alpha::NOT_ALPHA    => "ERROR: Name cannot contain non-alpha characters",
                          Zend_Validate_Alpha::STRING_EMPTY => "ERROR: Name can't be empty"
                      ))
            )
            ->addFilter('HtmlEntities')
            ->addFilter('StringTrim');

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('contact-email-address')
            ->setOptions(array('size' => '50'))
            ->setRequired(true)
            ->addValidator('NotEmpty', true)
            ->addValidator('EmailAddress', true)
            ->addFilter('HtmlEntities')
            ->addFilter('StringToLower')
            ->addFilter('StringTrim');

        $country = new Zend_Dojo_Form_Element_ComboBox('country');
        $country->setLabel('contact-country');
        $country->setOptions(
            array(
                 'autocomplete' => false,
                 'storeId'      => 'countryStore',
                 'storeType'    => 'dojo.data.ItemFileReadStore',
                 'storeParams'  => array('url' => "/default/contact/autocomplete"),
                 'dijitParams'  => array('searchAttr' => 'name')
            )
        )
            ->setRequired(true)
            ->addValidator('MotEmpty', true)
            ->addFilter('HTMLEntities')
            ->addFilter('StringToLower')
            ->addFilter('StringTrim');

        $message = new Zend_Form_Element_Textarea('message');
        $message->setLabel('contact-message')
            ->setOptions(array('rows' => '3', 'cols' => '40'))
            ->setRequired(true)
            ->addValidator('NotEmpty', true)
            ->addFilter('HtmlEntities')
            ->addFilter('StringTrim');

        $captcha = new Zend_Form_Element_Captcha('captcha',
            array(
                 'captcha' => 'Figlet',
                 'wordLen' => 3,
                 'timeout' => 300,
                 'width'   => 300,
                 'height'  => 100
            )
        );
        $captcha->setLabel('contact-verification');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('contact-send-message')
            ->setOptions(array('class' => 'button radius'));

        $this->addElement($name)
            ->addElement($email)
            ->addElement($country)
            ->addElement($message)
            ->addElement($captcha)
            ->addElement($submit);

    }
}