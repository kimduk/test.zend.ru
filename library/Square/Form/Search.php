<?php
class Square_Form_search extends Zend_Form
{
    public $messages
        = array(
            Zend_Validate_Int::INVALID => '\'%value%\' is not an integer',
            Zend_Validate_Int::NOT_INT => '\'%value%\' is not an integer'
        );

    public function init()
    {
        $this->setAction('/catalog/item/search')->setMethod('get');

        $query = new Zend_Form_Element_Text('q');
        $query->setLabel('Keywords: ')
            ->setAttrib('required', '1')
            ->setOptions(array('size' => '20'))
            ->addFilter('HtmlEntities')
            ->addFilter('StringTrim');
        $query->setDecorators(
            array(
                 array('ViewHelper'),
                 array('Errors'),
                 array('Label', array('tag' => '<span>'))
            )
        );

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Search')
            ->setOptions(array('class' => 'button radius'));
        $submit->setDecorators(
            array(
                 array('ViewHelper')
            )
        );

        $this->addElement($query)
            ->addElement($submit);
    }


}