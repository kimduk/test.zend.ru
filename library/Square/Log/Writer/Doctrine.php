<?php
class Square_Log_Writer_Doctrine extends Zend_Log_Writer_Abstract
{
    protected $_modelName,
        $_columnMap;

    public function __construct($modelName, $columnMap)
    {
        $this->_modelName = $modelName;
        $this->_columnMap = $columnMap;
    }

    public function setFormatter($formatter)
    {
        require_once 'Zend/log/Exception.php';
        throw new Zend_Log_Exception(get_class() . ' does not support formatting');
    }

    protected function _write($message)
    {
        $data = array();
        foreach ($this->_columnMap as $messageField => $modelField) {
            $data[$modelField] = $message[$messageField];
        }

        $model = new $this->_modelName();
        $model->fromArray($data);
        $model->save();
    }

    static public function factory($config)
    {
        return new self(self::_perseConfig($config));
    }
}